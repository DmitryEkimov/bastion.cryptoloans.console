﻿/// https://celsius.network/crypto-loans
using System;

namespace Bastion.CryptoLoans.Console
{
    class Program
    {
        /// цена битка должна быть настоящей
        /// так что здесь расчеты могут малость отличаться от того,
        /// что на сайте цельсиуса, но не на много
        const decimal BTC_HARDCODED_PRICE = 47101.0m;
        const decimal ETH_HARDCODED_PRICE = 3015.0m;

        /// Процент скидки, которая предоставляется на комиссию
        /// если выбрал оплату их собсвтенными монетками
        const decimal MIGOM_CRYPTO_COIN_DISCOUNT_PERCENT = 25.0m;

        enum Ltv
        {
            Ltv25,
            Ltv33,
            Ltv50,
        }



        static void Main(string[] args)
        {
            var res = _calculateLoan(
                                ltv: Ltv.Ltv50,
                                loanValue: 100.0m,
                                numMonths: 12.0m,
                                collateralCurrencyId: "BTC",
                                paidWithMigomCryptoCoins: true
            );
            System.Console.WriteLine(res.ToString());
        }
        public class LoanResult
        {
            public decimal monthlyInterest;
            public decimal totalInterest;
            public decimal loanValue;
            public decimal numMonths;
            public decimal ltvLoanPercent;
            public decimal ltvValuePercent;
            public decimal collateralValue;
            public string collateralCurrencyId;
            public bool payWithMigomCryptoCoins;
            public override string ToString()
            => $"Pay with Migom Crypto Coins: {payWithMigomCryptoCoins}\r\n" +
                $"Monthly interest: {monthlyInterest:n2}\r\n" +
                $"Total interest: {totalInterest:n2}\r\n" +
                $"Collateral: {collateralValue:n4} {collateralCurrencyId}\r\n" +
                $"LTV loan percent: {ltvLoanPercent:n2}\r\n" +
                $"LTV value percent: ${ ltvValuePercent:n2}\r\n" +
                $"Loan period(months): ${numMonths:n2}";
        }

        /// [ltv] loan to value, соотношение объема кредита
        /// к объему залога (collateral)
        /// [loanValue] общий объем кредита,
        /// который юзер хочет взять
        /// [numMonths] количество месяцев. Оно не может быть
        /// меньше, чем [minLoanPeriodMonth] или больше, чем
        /// [maxLoanPeriodMonths].
        /// [minMonthStep] число месяцев, до которого
        /// происходит округление. Если передать, например, 8.0, то
        /// оно округлится до 6.0, а вот 10.0 уже округлится до 12.0
        /// [paidWithMigomCryptoCoins] аналог цельсиевского Paid CEL
        /// по сути, просто убавляет процент комиссии на четверть
        static LoanResult _calculateLoan(
                Ltv ltv,
                decimal loanValue,
                decimal numMonths,
                string collateralCurrencyId,
                decimal maxLoanPeriodMonths = 12.0m * 3.0m,
                decimal minLoanPeriodMonth = 6.0m,
                decimal minMonthStep = 6.0m,
                bool paidWithMigomCryptoCoins = false)
        {
            var _numMonths = numMonths;
            if (_numMonths < 1.0m)
                _numMonths = 1.0m;
            _numMonths = _roundToNearest(
            value: clamp(numMonths,
                minLoanPeriodMonth,
                maxLoanPeriodMonths
            ),
            roundTo: minMonthStep
            );
            var ltvLoanPercent = _getLtvLoanPercent(ltv);
            var ltvValuePercent = _getLtvValuePercent(ltv);
            var collateralValue = _getCollateralValue(
                ltv: ltv,
                ltvCollateralValuePercent: ltvValuePercent,
                collateralCurrencyId: collateralCurrencyId,
                loanValue: loanValue
            );
            var percent = ltvLoanPercent / 100.0m;
            var discountMultiplier = _getInterestDiscountMultiplier(paidWithMigomCryptoCoins);

            var monthlyInterest = _roundToNearest(
                value: (loanValue * (percent * discountMultiplier)) / 12.0m,
                roundTo: .01m
            );
            var totalInterest = _roundToNearest(value: _numMonths * monthlyInterest);

            return new LoanResult()
            {
                loanValue = loanValue,
                monthlyInterest = monthlyInterest,
                totalInterest = totalInterest,
                numMonths = _numMonths,
                ltvLoanPercent = ltvLoanPercent,
                ltvValuePercent = ltvValuePercent,
                collateralValue = collateralValue,
                collateralCurrencyId = collateralCurrencyId,
                payWithMigomCryptoCoins = paidWithMigomCryptoCoins
            };
        }

        static decimal clamp(decimal numMonths, decimal minLoanPeriodMonth, decimal maxLoanPeriodMonths)
        {
            if (numMonths < minLoanPeriodMonth)
                return minLoanPeriodMonth;
            if (numMonths > minLoanPeriodMonth)
                return maxLoanPeriodMonths;
            return numMonths;
        }

        static decimal _roundToNearest(decimal value, decimal roundTo = .01m)
        => Math.Round(value / roundTo) * roundTo;

        static decimal _getLtvLoanPercent(Ltv ltv)
        => ltv switch
        {
            Ltv.Ltv25 => 1.0m,
            Ltv.Ltv33 => 6.95m,
            Ltv.Ltv50 => 8.95m,
            _ => throw new ArgumentOutOfRangeException(nameof(ltv))
        };

        static decimal _getLtvValuePercent(Ltv ltv)
        => ltv switch
        {
            Ltv.Ltv25 => 25.0m,
            Ltv.Ltv33 => 33.0m,
            Ltv.Ltv50 => 50.0m,
            _ => throw new ArgumentOutOfRangeException(nameof(ltv))
        };
        static decimal _getInterestDiscountMultiplier(bool paidWithMigomCryptoCoins)
        {
            var migomMultiplier = 1.0m - (MIGOM_CRYPTO_COIN_DISCOUNT_PERCENT / 100.0m);
            return paidWithMigomCryptoCoins ? migomMultiplier : 1.0m;
        }

        static decimal _getCollateralValue(
          Ltv ltv,
          decimal ltvCollateralValuePercent,
          string collateralCurrencyId,
          decimal loanValue
        )
        {
            decimal price = collateralCurrencyId switch
            {
                "BTC" => BTC_HARDCODED_PRICE,
                "ETH" => ETH_HARDCODED_PRICE,
                _ => 0.0m
            };

            var percent = ltvCollateralValuePercent / 100.0m;

            return loanValue / (price * percent);
        }


    }
}
